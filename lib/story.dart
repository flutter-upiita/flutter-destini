import 'package:destini_challenge_starting/main.dart';

class Story {
  String storyTitle;
  String choice1;
  String choice2;

  //This is an effective dart language use, we assume that our parameters are the same type as the properties
  Story({this.storyTitle, this.choice1, this.choice2});

  //{} -> put braces 'cause the arguments are named, not positional
}
